let name = '';
let age = '';

do {
    name = prompt('Enter your name:', name);
    age = parseInt(prompt('Enter your age:', age));

    if (isNaN(age) || age < 0) {
        alert('Please enter a valid age!');
        age = '';
    }
    else if (!/^[a-zA-Z]+$/.test(name)) {
        alert('Please enter a valid name!');
        name = '';
    }
} while (!name || !age);

if (age < 18) {
    alert('You are not allowed to visit this website.');
} else if (age >= 18 && age <= 22) {
    const result = confirm('Are you sure you want to continue?');
    if (result) {
        alert(`Welcome, ${name}!`);
    } else {
        alert('You are not allowed to visit this website.');
    }
} else {
    alert(`Welcome, ${name}!`);
}